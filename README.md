# Noleme Amaebi (甘エビ)

_Last updated for v0.7.2_

This library is meant as a set of opinionated utility features for building lightweight web services, built on top of [undertow](http://undertow.io).

It should be embeddable pretty much anywhere, but its original goal was to be embedded in a noleme-console instance.
It should be noted that although it was designed for use for the Noleme ecosystem, anything defined here should remain project-agnostic, see noleme-amaebi-rest for Noleme-centric implementations.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-amaebi</artifactId>
    <version>0.7.2</version>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

This is what an extremely barebones Amaebi setup can look like:

```java
var server = new Amaebi(new AmaebiConfiguration("localhost", 9000));

server.routing()
    .register(Method.GET, "/hello", ex -> send(ex, "Hello world!"))
    .register(Method.POST, "/goodbye", ex -> send(ex, "Goodbye world.", Status.ACCEPTED))
;

server.build().start();
```

```shell
➜ curl localhost:9000/hello
Hello world!
➜ curl -X POST localhost:9000/goodbye
Goodbye world.
```

A similar functionality can be structured into controllers, like so:

```java
@RoutePrefix(prefix = "/world")
public class MyController
{
    @Route(method = Method.GET, path = "/hello")
    public void helloWorld(HttpServerExchange ex)
    {
        send(ex, "Hello world!");
    }

    @Route(method = Method.POST, path = "/goodbye")
    public void goodByeWorld(HttpServerExchange ex)
    {
        send(ex, "Goodbye world.");
    }
}
```

```java
/* Same as above, but this time we register MyController instead */
server.routing()
    .register(new MyController())
;
```

```shell
➜ curl localhost:9000/world/hello
Hello world!
➜ curl -X POST localhost:9000/world/goodbye
Goodbye world.
```

Features that will need to be documented:
* More flavours for registering routes
* Request and Controller handlers
* Error handlers
* Handler-based custom annotations
* `Controller` utility class

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
