package com.noleme.amaebi.routing;

import com.noleme.amaebi.http.Method;

import java.util.Objects;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class Route
{
    private final Method method;
    private final String path;

    /**
     *
     * @param method
     * @param path
     */
    public Route(Method method, String path)
    {
        this.method = method;
        this.path = path;
    }

    public Method getMethod()
    {
        return this.method;
    }

    public String getPath()
    {
        return this.path;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Route route = (Route) o;
        return this.method == route.method
            && Objects.equals(this.path, route.path)
        ;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(this.method, this.path);
    }
}
