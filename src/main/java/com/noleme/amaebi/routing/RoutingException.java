package com.noleme.amaebi.routing;

import com.noleme.amaebi.exception.AmaebiException;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class RoutingException extends AmaebiException
{
    /**
     *
     * @param message
     */
    public RoutingException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public RoutingException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
