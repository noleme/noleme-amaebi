package com.noleme.amaebi.routing;

import com.noleme.amaebi.controller.annotation.Blocking;
import com.noleme.amaebi.controller.annotation.RoutePrefix;
import com.noleme.amaebi.handler.ChainableHttpHandler;
import com.noleme.amaebi.handler.controller.ControllerHandler;
import com.noleme.amaebi.handler.controller.ControllerRequestHandler;
import com.noleme.amaebi.http.Method;
import com.noleme.commons.container.Pair;
import com.noleme.commons.container.Triplet;
import io.undertow.server.HttpHandler;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.server.handlers.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class Routing
{
    /**
     * It is worth noting that the Method in the Triplets can be null in case of a registration call via either {@link #register(Route, HttpHandler)} or {@link #register(Method, String, HttpHandler)}
     * which in practice means a direct registration of an HttpHandler via an instance, likely a lambda or a method reference.
     */
    private final List<Triplet<Route, HttpHandler, java.lang.reflect.Method>> routeHandlers;
    private final List<Pair<Class<? extends Throwable>, HttpHandler>> errorHandlers;
    private final List<HttpHandler> requestHandlers;
    private final List<ControllerHandler> controllerHandlers;
    private HttpHandler notFoundHandler;
    private HttpHandler compiled;

    private static final Logger logger = LoggerFactory.getLogger(Routing.class);

    public Routing()
    {
        this.routeHandlers = new ArrayList<>();
        this.errorHandlers = new ArrayList<>();
        this.requestHandlers = new ArrayList<>();
        this.controllerHandlers = new ArrayList<>();
        this.compiled = null;
    }

    /**
     *
     * @param route
     * @param handler
     * @param method
     * @return
     */
    protected Routing register(Route route, HttpHandler handler, java.lang.reflect.Method method)
    {
        this.routeHandlers.add(new Triplet<>(route, handler, method));

        logger.debug(
            "Registered route {} {} {} (blocking: {})",
            route.getMethod().name(),
            route.getPath(),
            (method != null ? "from "+method.getDeclaringClass().getName()+"."+method.getName() + " " : ""),
            (handler instanceof BlockingHandler)
        );

        return this;
    }

    /**
     *
     * @param route
     * @param handler
     * @return
     */
    public Routing register(Route route, HttpHandler handler)
    {
        return this.register(route, handler, null);
    }

    /**
     *
     * @param method
     * @param path
     * @param handler
     * @return
     */
    public Routing register(Method method, String path, HttpHandler handler)
    {
        return this.register(new Route(method, path), handler);
    }

    /**
     *
     * @param controller
     * @return
     */
    public <C> Routing register(C controller)
    {
        return this.register(controller, "");
    }

    /**
     *
     * @param controller
     * @param prefix
     * @return
     */
    public <C> Routing register(C controller, String prefix)
    {
        RoutePrefix prefixAnnotation = controller.getClass().getAnnotation(RoutePrefix.class);
        if (prefixAnnotation != null)
            prefix = prefix+prefixAnnotation.prefix();

        logger.debug("Scanning routes in {} (prefix: \"{}\")", controller.getClass().getName(), prefix);

        for (java.lang.reflect.Method m : controller.getClass().getMethods())
        {
            for (com.noleme.amaebi.controller.annotation.Route route : m.getAnnotationsByType(com.noleme.amaebi.controller.annotation.Route.class))
            {
                HttpHandler handler = (exchange) -> {
                    try {
                        m.invoke(controller, exchange);
                    }
                    catch (InvocationTargetException e) {
                        if (e.getCause() instanceof Exception)
                            throw (Exception) e.getCause();
                        throw new RuntimeException("An unexpected error occurred.", e.getCause());
                    }
                };

                Blocking blocking = m.getAnnotation(Blocking.class);
                if (blocking != null)
                    handler = new BlockingHandler(handler);

                this.register(new Route(route.method(), prefix+route.path()), handler, m);
            }
        }

        return this;
    }

    /**
     *
     * @return
     */
    public HttpHandler handler()
    {
        return this.compiled;
    }

    /**
     *
     * @return
     */
    synchronized public Routing build()
    {
        RoutingHandler routingHandler = new RoutingHandler();
        for (Triplet<Route, HttpHandler, java.lang.reflect.Method> routeHandler : this.routeHandlers)
        {
            HttpHandler handler;

            /*
             * If the returned handler is an instance of BlockingHandler, it will be executed as a Runnable by the worker pool.
             * In order to preserve error handling and controller-related handling, we wrap the handler with all the required layers of control.
             */
            if (routeHandler.second instanceof BlockingHandler)
            {
                HttpHandler targetHandler = ((BlockingHandler) routeHandler.second).getHandler();
                handler = new BlockingHandler(this.wrapForExceptions(this.wrapForController(targetHandler, routeHandler.third)));
            }
            else
                handler = this.wrapForController(routeHandler.second, routeHandler.third);

            routingHandler.add(routeHandler.first.getMethod().name(), routeHandler.first.getPath(), handler);
        }

        if (this.notFoundHandler != null)
            routingHandler.setFallbackHandler(this.notFoundHandler);

        HttpHandler finalHandler = routingHandler;

        for (int i = this.requestHandlers.size() - 1 ; i >= 0 ; --i)
        {
            HttpHandler handler = this.requestHandlers.get(i);
            finalHandler = new ChainableHttpHandler(handler, finalHandler);
        }

        this.compiled = this.wrapForExceptions(finalHandler);

        return this;
    }

    /**
     *
     * @param handler
     * @return
     */
    private HttpHandler wrapForController(HttpHandler handler, java.lang.reflect.Method method)
    {
        HttpHandler finalHandler = handler;

        for (int i = this.controllerHandlers.size() - 1 ; i >= 0 ; --i)
        {
            ControllerHandler controllerHandler = this.controllerHandlers.get(i);
            finalHandler = new ChainableHttpHandler(new ControllerRequestHandler(method, controllerHandler), finalHandler);
        }

        return finalHandler;
    }

    /**
     *
     * @param handler
     * @return
     */
    private HttpHandler wrapForExceptions(HttpHandler handler)
    {
        ExceptionHandler finalHandler = new ExceptionHandler(handler);

        for (Pair<Class<? extends Throwable>, HttpHandler> errorHandler : this.errorHandlers)
            finalHandler.addExceptionHandler(errorHandler.first, errorHandler.second);

        return finalHandler;
    }

    /**
     *
     * @param handler
     * @return
     */
    public Routing setNotFoundHandler(HttpHandler handler)
    {
        this.notFoundHandler = handler;
        return this;
    }

    /**
     *
     * @param throwable
     * @param handler
     * @param <T>
     * @return
     */
    public <T extends Throwable> Routing addErrorHandler(Class<T> throwable, HttpHandler handler)
    {
        this.errorHandlers.add(new Pair<>(throwable, handler));
        return this;
    }

    /**
     *
     * @param handler
     * @return
     */
    public Routing addRequestHandler(HttpHandler handler)
    {
        this.requestHandlers.add(handler);
        return this;
    }

    /**
     *
     * @param handler
     * @return
     */
    public Routing addControllerHandler(ControllerHandler handler)
    {
        this.controllerHandlers.add(handler);
        return this;
    }
}
