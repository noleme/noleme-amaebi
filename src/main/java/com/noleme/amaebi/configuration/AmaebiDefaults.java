package com.noleme.amaebi.configuration;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 14/12/2019
 */
public enum AmaebiDefaults
{
    HOST("host", "localhost"),
    PORT("port", 9001),
    WORKER_THREADS("worker_threads", null),
    IO_THREADS("io_threads", null),
    ;

    private final String key;
    private final Object defaultValue;

    /**
     *
     * @param key
     * @param defaultValue
     */
    AmaebiDefaults(String key, Object defaultValue)
    {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public String getKey()
    {
        return this.key;
    }

    public Object getDefaultValue()
    {
        return this.defaultValue;
    }
}
