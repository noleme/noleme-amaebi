package com.noleme.amaebi.configuration;

import io.undertow.Undertow;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 14/12/2019
 */
public class AmaebiConfiguration
{
    private final Undertow.Builder builder;

    /**
     *
     */
    public AmaebiConfiguration()
    {
        this((String)AmaebiDefaults.HOST.getDefaultValue(), (Integer)AmaebiDefaults.PORT.getDefaultValue());
    }

    /**
     *
     * @param host
     * @param port
     */
    public AmaebiConfiguration(String host, int port)
    {
        this(new Configuration()
            .set(AmaebiDefaults.HOST.getKey(), host)
            .set(AmaebiDefaults.PORT.getKey(), port)
        );
    }

    /**
     *
     * @param conf
     */
    public AmaebiConfiguration(final Configuration conf)
    {
        this.builder = Undertow.builder();

        int port = conf.has(AmaebiDefaults.PORT.getKey()) ? conf.getInteger(AmaebiDefaults.PORT.getKey()) : (int) AmaebiDefaults.PORT.getDefaultValue();
        String host = conf.has(AmaebiDefaults.HOST.getKey()) ? conf.getString(AmaebiDefaults.HOST.getKey()) : (String) AmaebiDefaults.HOST.getDefaultValue();

        this.builder.addHttpListener(port, host);

        if (conf.has(AmaebiDefaults.IO_THREADS.getKey()))
            this.builder.setIoThreads(conf.getInteger(AmaebiDefaults.IO_THREADS.getKey()));
        if (conf.has(AmaebiDefaults.WORKER_THREADS.getKey()))
            this.builder.setWorkerThreads(conf.getInteger(AmaebiDefaults.WORKER_THREADS.getKey()));
    }

    public Undertow.Builder builder()
    {
        return this.builder;
    }
}
