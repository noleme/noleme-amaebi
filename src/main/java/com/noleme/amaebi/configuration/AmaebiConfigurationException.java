package com.noleme.amaebi.configuration;

import com.noleme.amaebi.exception.AmaebiException;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 22/05/15.
 */
public class AmaebiConfigurationException extends AmaebiException
{
    public AmaebiConfigurationException(String message) { super(message); }
    public AmaebiConfigurationException(String message, Throwable e) { super(message, e); }
}