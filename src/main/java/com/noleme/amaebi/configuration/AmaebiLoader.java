package com.noleme.amaebi.configuration;

import com.noleme.commons.file.Files;
import com.noleme.commons.file.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 27/01/2018
 */
public final class AmaebiLoader
{
    private AmaebiLoader() {}

    /**
     *
     * @param path
     * @return
     * @throws AmaebiConfigurationException
     */
    public static AmaebiConfiguration load(final String path) throws AmaebiConfigurationException
    {
        return load(path, new Configuration());
    }

    /**
     *
     * @param path
     * @return
     * @throws AmaebiConfigurationException
     */
    public static AmaebiConfiguration load(final String path, final Configuration conf) throws AmaebiConfigurationException
    {
        try (InputStream in = streamFromFileOrResource(path))
        {
            Properties props = new Properties();
            props.load(in);

            return new AmaebiConfiguration(load(props, conf));
        }
        catch (IOException e) {
            throw new AmaebiConfigurationException("An error occurred while attempting to parse file in path "+path+".", e);
        }
    }

    /**
     *
     * @param path
     * @return
     * @throws AmaebiConfigurationException
     */
    private static InputStream streamFromFileOrResource(final String path) throws AmaebiConfigurationException
    {
        try {
            if (Files.fileExists(path))
                return Files.streamFrom(path);
            else if (Resources.exists(path))
                return Resources.streamFrom(path);

            throw new AmaebiConfigurationException("No file nor resource could be found for path "+path);
        }
        catch (IOException e) {
            throw new AmaebiConfigurationException("An error occurred while attempting to load file or resource at "+path, e);
        }
    }
    
    /**
     *
     * @param props
     * @param config
     * @return
     */
    private static Configuration load(final Properties props, final Configuration config)
    {
        for (Map.Entry<Object, Object> e : props.entrySet())
        {
            String key = (String)e.getKey();
            String value = (String)e.getValue();
            if (isInteger(value))
                config.set(key, Integer.parseInt(value));
            else if (isNumeric(value))
                config.set(key, Double.parseDouble(value));
            else if (isBoolean(value))
                config.set(key, Boolean.parseBoolean(value));
            else
                config.set(key, value);
        }

        return config;
    }

    private static boolean isInteger(String str)
    {
        return str.matches("^-?\\d+$");
    }

    private static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    private static boolean isBoolean(String str)
    {
        return str.equals("true") || str.equals("false");
    }
}
