package com.noleme.amaebi.wrapper;

import com.noleme.amaebi.exception.ClientException;
import com.noleme.commons.container.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 28/06/2018
 */
public interface UpdateUnwrapper<I, O>
{
    /**
     *
     * @param input
     * @param preexisting
     * @return
     * @throws ClientException
     */
    O unwrap(I input, O preexisting) throws ClientException;

    /**
     *
     * @param input
     * @return
     * @throws ClientException
     */
    default List<O> unwrap(List<Pair<I, O>> input) throws ClientException
    {
        ArrayList<O> array = new ArrayList<>();
        for (Pair<I, O> item : input)
            array.add(this.unwrap(item.first, item.second));
        return array;
    }
}
