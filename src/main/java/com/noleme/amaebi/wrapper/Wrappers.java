package com.noleme.amaebi.wrapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.exception.ServerException;
import com.noleme.json.Json;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 28/06/2018
 */
public final class Wrappers
{
    public static final DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(TimeZone.getTimeZone("UTC").toZoneId());


    /**
     *
     * @param date
     * @return
     */
    public static String wrap(Date date)
    {
        return wrap(date.toInstant());
    }

    /**
     *
     * @param instant
     * @return
     */
    public static String wrap(Instant instant)
    {
        return wrap(instant, DEFAULT_DATE_FORMAT);
    }

    /**
     *
     * @param date
     * @param format
     * @return
     */
    public static String wrap(Date date, DateTimeFormatter format)
    {
        return wrap(date.toInstant(), format);
    }

    /**
     *
     * @param instant
     * @param format
     * @return
     */
    public static String wrap(Instant instant, DateTimeFormatter format)
    {
        if (instant == null)
            return null;

        return format.format(instant);
    }

    /**
     *
     * @param map
     * @param <T>
     * @return
     */
    public static <T> ObjectNode wrap(Map<String, T> map) throws ServerException
    {
        if (map == null)
            return null;

        ObjectNode json = Json.newObject();

        for (Map.Entry<String, T> e : map.entrySet())
        {
            String key = e.getKey();
            T value = e.getValue();

            if (value instanceof Collection)
                json.set(key, wrap((Collection)value, Json::toJson));
            else
                json.put(key, value != null ? value.toString() : null);
        }

        return json;
    }

    /**
     *
     * @param map
     * @param wrapper
     * @param <J>
     * @param <T>
     * @return
     */
    public static <J extends JsonNode, T> ObjectNode wrap(Map<String, T> map, Wrapper<T, J> wrapper) throws ServerException
    {
        if (map == null)
            return null;

        ObjectNode json = Json.newObject();

        for (Map.Entry<String, T> e : map.entrySet())
            json.set(e.getKey(), wrapper.wrap(e.getValue()));

        return json;
    }

    /**
     *
     * @param collection
     * @param wrapper
     * @param <J>
     * @param <T>
     * @return
     */
    public static <J extends JsonNode, T> ArrayNode wrap(Collection<T> collection, Wrapper<T, J> wrapper) throws ServerException
    {
        if (collection == null)
            return null;

        ArrayNode json = Json.newArray();

        for (T item : collection)
            json.add(wrapper.wrap(item));

        return json;
    }

    /**
     * Note for my future self (for after barely two days I had already forgot why the hell this is split into an army
     * of super-specialized methods), the jackson API does not allow the addition of values with undetermined type.
     * And since type gating can't be done via generics, we're stuck with this shit.
     *
     * @param strings
     * @return
     */
    public static ArrayNode stringArray(Collection<String> strings)
    {
        ArrayNode array = Json.newArray();
        for (String string : strings)
            array.add(string);
        return array;
    }

    /**
     *
     * @param integers
     * @return
     */
    public static ArrayNode integerArray(Collection<Integer> integers)
    {
        ArrayNode array = Json.newArray();
        for (Integer integer : integers)
            array.add(integer);
        return array;
    }

    /**
     *
     * @param doubles
     * @return
     */
    public static ArrayNode doubleArray(Collection<Double> doubles)
    {
        ArrayNode array = Json.newArray();
        for (Double d : doubles)
            array.add(d);
        return array;
    }

    /**
     *
     * @param booleans
     * @return
     */
    public static ArrayNode booleanArray(Collection<Boolean> booleans)
    {
        ArrayNode array = Json.newArray();
        for (Boolean b : booleans)
            array.add(b);
        return array;
    }

    /**
     *
     * @param enums
     * @param <E>
     * @return
     */
    public static <E extends Enum<E>> ArrayNode enumArray(Collection<E> enums)
    {
        ArrayNode array = Json.newArray();
        for (E e : enums)
            array.add(e.name());
        return array;
    }

    /**
     * Note for my future self: same shit as for *Array methods.
     *
     * @param collection
     * @param jsonArray
     * @param <C>
     * @return
     */
    public static <C extends Collection<String>> C stringCollection(C collection, ArrayNode jsonArray)
    {
        for (JsonNode o : jsonArray)
            collection.add(o.asText());
        return collection;
    }

    /**
     *
     * @param collection
     * @param jsonArray
     * @param <C>
     * @return
     */
    public static <C extends Collection<Integer>> C integerCollection(C collection, ArrayNode jsonArray)
    {
        for (JsonNode o : jsonArray)
            collection.add(o.asInt());
        return collection;
    }

    /**
     *
     * @param collection
     * @param jsonArray
     * @param <C>
     * @return
     */
    public static <C extends Collection<Double>> C doubleCollection(C collection, ArrayNode jsonArray)
    {
        for (JsonNode o : jsonArray)
            collection.add(o.asDouble());
        return collection;
    }

    /**
     *
     * @param collection
     * @param jsonArray
     * @param <C>
     * @return
     */
    public static <C extends Collection<Boolean>> C booleanCollection(C collection, ArrayNode jsonArray)
    {
        for (JsonNode o : jsonArray)
            collection.add(o.asBoolean());
        return collection;
    }

    /**
     *
     * @param collection
     * @param type
     * @param jsonArray
     * @param <C>
     * @param <E>
     * @return
     */
    public static <C extends Collection<E>, E extends Enum<E>> C enumCollection(C collection, Class<E> type, ArrayNode jsonArray)
    {
        for (JsonNode o : jsonArray)
            collection.add(Enum.valueOf(type, o.asText()));
        return collection;
    }

    /**
     *
     * @param collection
     * @param jsonArray
     * @param unwrapper
     * @param <C>
     * @param <T>
     * @return
     */
    public static <C extends Collection<T>, T> C customCollection(C collection, ArrayNode jsonArray, Unwrapper<JsonNode, T> unwrapper) throws ClientException
    {
        for (JsonNode o : jsonArray)
            collection.add(unwrapper.unwrap(o));
        return collection;
    }
}
