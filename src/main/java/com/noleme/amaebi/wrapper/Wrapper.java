package com.noleme.amaebi.wrapper;

import com.noleme.amaebi.exception.ServerException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 28/06/2018
 */
public interface Wrapper <I, O>
{
    /**
     *
     * @param input
     * @return
     * @throws ServerException
     */
    O wrap(I input) throws ServerException;

    /**
     *
     * @param input
     * @return
     * @throws ServerException
     */
    default List<O> wrap(Collection<I> input) throws ServerException
    {
        List<O> list = new ArrayList<>();
        for (I i : input)
            list.add(this.wrap(i));
        return list;
    }
}
