package com.noleme.amaebi.wrapper;

import com.noleme.amaebi.exception.ClientException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Pierre Lecerf (pierre.lecerf@gmail.com)
 * Created on 28/06/2018
 */
public interface Unwrapper <I, O>
{
    /**
     *
     * @param input
     * @return
     * @throws ClientException
     */
    O unwrap(I input) throws ClientException;

    /**
     *
     * @param input
     * @return
     * @throws ClientException
     */
    default List<O> unwrap(Collection<I> input) throws ClientException
    {
        ArrayList<O> array = new ArrayList<>();
        for (I item : input)
            array.add(this.unwrap(item));
        return array;
    }
}
