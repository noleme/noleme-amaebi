package com.noleme.amaebi.wrapper.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.noleme.amaebi.exception.ServerException;
import com.noleme.amaebi.wrapper.Wrapper;
import com.noleme.json.Json;

import java.util.Collection;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface JsonWrapper <I> extends Wrapper<I, JsonNode>
{
    /**
     *
     * @param input
     * @return
     * @throws ServerException
     */
    default ArrayNode wrapJson(Collection<I> input) throws ServerException
    {
        ArrayNode array = Json.newArray();
        for (I i : input)
            array.add(this.wrap(i));
        return array;
    }
}
