package com.noleme.amaebi.wrapper.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.wrapper.Unwrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface JsonUnwrapper <O> extends Unwrapper<JsonNode, O>
{
    /**
     *
     * @param input
     * @return
     */
    default List<O> unwrap(ArrayNode input) throws ClientException
    {
        ArrayList<O> array = new ArrayList<>();
        for (JsonNode item : input)
            array.add(this.unwrap(item));
        return array;
    }
}
