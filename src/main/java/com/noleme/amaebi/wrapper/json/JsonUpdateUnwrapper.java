package com.noleme.amaebi.wrapper.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.wrapper.UpdateUnwrapper;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface JsonUpdateUnwrapper <O> extends UpdateUnwrapper<JsonNode, O>
{
}
