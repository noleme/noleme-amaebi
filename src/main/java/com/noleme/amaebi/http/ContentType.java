package com.noleme.amaebi.http;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 08/11/2020
 */
public enum ContentType
{
    TextPlain("text/plain"),
    TextHtml("text/html"),
    ApplicationOctetStream("application/octet-stream"),
    ApplicationJson("application/json"),
    MultipartFormData("multipart/form-data"),;

    private final String mimeType;

    ContentType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getMimeType()
    {
        return this.mimeType;
    }
}
