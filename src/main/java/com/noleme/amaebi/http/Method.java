package com.noleme.amaebi.http;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public enum Method
{
    GET,
    POST,
    PATCH,
    PUT,
    DELETE,
    OPTIONS
    ;
}
