package com.noleme.amaebi.handler;

import com.noleme.amaebi.exception.NotFoundException;
import com.noleme.amaebi.http.Status;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 16/12/2019
 */
public class NotFoundHandler implements HttpHandler
{
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception
    {
        throw new NotFoundException(Status.NOT_FOUND.getMessage());
    }
}
