package com.noleme.amaebi.handler;

import com.noleme.amaebi.response.ErrorHandler;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 16/12/2019
 */
public class ExceptionHandler <E extends Throwable> implements HttpHandler
{
    private final ErrorHandler<E> errorHandler;

    /**
     *
     * @param errorHandler
     */
    public ExceptionHandler(ErrorHandler<E> errorHandler)
    {
        this.errorHandler = errorHandler;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange)
    {
        E exception = (E) exchange.getAttachment(io.undertow.server.handlers.ExceptionHandler.THROWABLE);
        this.errorHandler.error(exchange, exception);
    }
}
