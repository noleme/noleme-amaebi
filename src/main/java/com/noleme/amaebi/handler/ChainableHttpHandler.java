package com.noleme.amaebi.handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public final class ChainableHttpHandler implements HttpHandler
{
    private final HttpHandler current;
    private final HttpHandler next;

    /**
     *
     * @param current
     * @param next
     */
    public ChainableHttpHandler(HttpHandler current, HttpHandler next)
    {
        this.current = current;
        this.next = next;
    }

    /**
     *
     * @param exchange
     * @throws Exception
     */
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception
    {
        this.current.handleRequest(exchange);
        this.next.handleRequest(exchange);
    }
}
