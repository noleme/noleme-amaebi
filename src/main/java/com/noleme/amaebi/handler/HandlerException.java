package com.noleme.amaebi.handler;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class HandlerException extends Exception
{
    /**
     *
     * @param message
     */
    public HandlerException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public HandlerException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
