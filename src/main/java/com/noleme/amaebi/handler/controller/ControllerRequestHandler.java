package com.noleme.amaebi.handler.controller;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.lang.reflect.Method;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public final class ControllerRequestHandler implements HttpHandler
{
    private final Method controllerMethod;
    private final ControllerHandler handler;

    /**
     *
     * @param controllerMethod
     * @param handler
     */
    public ControllerRequestHandler(Method controllerMethod, ControllerHandler handler)
    {
        this.controllerMethod = controllerMethod;
        this.handler = handler;
    }

    @Override
    public final void handleRequest(HttpServerExchange exchange) throws Exception
    {
        this.handler.handleRequest(exchange, this.controllerMethod);
    }
}
