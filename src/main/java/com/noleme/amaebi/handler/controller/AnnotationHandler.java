package com.noleme.amaebi.handler.controller;

import com.noleme.amaebi.handler.HandlerException;
import io.undertow.server.HttpServerExchange;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public abstract class AnnotationHandler <A extends Annotation> implements ControllerHandler
{
    private final Class<A> annotationType;

    /**
     * @param annotationType
     */
    public AnnotationHandler(Class<A> annotationType)
    {
        this.annotationType = annotationType;
    }

    /**
     *
     * @param exchange
     * @param controllerMethod
     * @throws Exception
     */
    public void handleRequest(HttpServerExchange exchange, Method controllerMethod) throws Exception
    {
        /* This can happen if the method was registered as a direct HttpHandler, a lambda or method reference */
        if (controllerMethod == null)
            return;
        for (A annotation : controllerMethod.getAnnotationsByType(this.annotationType))
            this.handleAnnotation(exchange, controllerMethod, annotation);
    }

    /**
     *
     * @param exchange
     * @param controllerMethod
     * @throws HandlerException
     */
    public abstract void handleAnnotation(HttpServerExchange exchange, Method controllerMethod, A annotation) throws Exception;
}
