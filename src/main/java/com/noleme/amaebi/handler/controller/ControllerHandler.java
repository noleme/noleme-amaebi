package com.noleme.amaebi.handler.controller;

import com.noleme.amaebi.handler.HandlerException;
import io.undertow.server.HttpServerExchange;

import java.lang.reflect.Method;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface ControllerHandler
{
    /**
     *
     * @param exchange
     * @param controllerMethod If the handler was registered as part of a controller object, the method instance will be passed ; otherwise this argument is null (eg. in the case of a route registered with a direct handler, lambda or method reference)
     * @throws HandlerException
     */
    void handleRequest(HttpServerExchange exchange, Method controllerMethod) throws Exception;
}
