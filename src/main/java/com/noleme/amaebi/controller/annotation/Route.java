package com.noleme.amaebi.controller.annotation;

import com.noleme.amaebi.http.Method;

import java.lang.annotation.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(Routes.class)
public @interface Route
{
    Method method();
    String path() default "";
}
