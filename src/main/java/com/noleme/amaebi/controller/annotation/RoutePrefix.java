package com.noleme.amaebi.controller.annotation;

import java.lang.annotation.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 16/12/2019
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RoutePrefix
{
    String prefix();
}
