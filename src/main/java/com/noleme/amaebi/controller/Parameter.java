package com.noleme.amaebi.controller;

import com.noleme.amaebi.exception.ClientException;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public final class Parameter
{
    private final String name;
    private final String rawValue;

    /**
     *
     * @param name
     * @param rawValue
     */
    public Parameter(String name, String rawValue)
    {
        this.name = name;
        this.rawValue = rawValue;
    }

    /**
     * 
     * @return
     */
    public String name()
    {
        return this.name;
    }

    /**
     * 
     * @return
     */
    public boolean isNull()
    {
        return this.rawValue == null;
    }

    /**
     * 
     * @return
     */
    public String asString()
    {
        return this.rawValue;
    }

    /**
     * 
     * @param defaultValue
     * @return
     */
    public String asString(String defaultValue)
    {
        return this.rawValue != null ? this.rawValue : defaultValue;
    }

    /**
     * 
     * @return
     * @throws ClientException
     */
    public Integer asInt() throws ClientException
    {
        return this.asInt(null);
    }

    /**
     * 
     * @param defaultValue
     * @return
     * @throws ClientException
     */
    public Integer asInt(Integer defaultValue) throws ClientException
    {
        return this.as(input -> {
            try {
                return Integer.parseInt(input);
            }
            catch (NumberFormatException e) {
                throw new ClientException("The provided value for \""+this.name()+"\" could not be interpreted as a valid integer.", e);
            }
        }, defaultValue);
    }

    /**
     * 
     * @return
     * @throws ClientException
     */
    public Long asLong() throws ClientException
    {
        return this.asLong(null);
    }

    /**
     * 
     * @param defaultValue
     * @return
     * @throws ClientException
     */
    public Long asLong(Long defaultValue) throws ClientException
    {
        return this.as(input -> {
            try {
                return Long.parseLong(input);
            }
            catch (NumberFormatException e) {
                throw new ClientException("The provided value for \""+this.name()+"\" could not be interpreted as a valid long.", e);
            }
        }, defaultValue);
    }

    /**
     *
     * @return
     * @throws ClientException
     */
    public Float asFloat() throws ClientException
    {
        return this.asFloat(null);
    }

    /**
     *
     * @param defaultValue
     * @return
     * @throws ClientException
     */
    public Float asFloat(Float defaultValue) throws ClientException
    {
        return this.as(input -> {
            try {
                return Float.parseFloat(input);
            }
            catch (NumberFormatException e) {
                throw new ClientException("The provided value for \""+this.name()+"\" could not be interpreted as a valid float.", e);
            }
        }, defaultValue);
    }

    /**
     *
     * @return
     * @throws ClientException
     */
    public Double asDouble() throws ClientException
    {
        return this.asDouble(null);
    }

    /**
     *
     * @param defaultValue
     * @return
     * @throws ClientException
     */
    public Double asDouble(Double defaultValue) throws ClientException
    {
        return this.as(input -> {
            try {
                return Double.parseDouble(input);
            }
            catch (NumberFormatException e) {
                throw new ClientException("The provided value for \""+this.name()+"\" could not be interpreted as a valid double.", e);
            }
        }, defaultValue);
    }

    /**
     * 
     * @return
     * @throws ClientException
     */
    public Boolean asBoolean() throws ClientException
    {
        return this.asBoolean(null);
    }

    /**
     * 
     * @param defaultValue
     * @return
     * @throws ClientException
     */
    public Boolean asBoolean(Boolean defaultValue) throws ClientException
    {
        return this.as(Boolean::parseBoolean, defaultValue);
    }

    /**
     *
     * @param type
     * @param <E>
     * @return
     * @throws ClientException
     */
    public <E extends Enum<E>> E asEnum(Class<E> type) throws ClientException
    {
        return this.asEnum(type, null);
    }

    /**
     *
     * @param type
     * @param defaultValue
     * @param <E>
     * @return
     * @throws ClientException
     */
    public <E extends Enum<E>> E asEnum(Class<E> type, E defaultValue) throws ClientException
    {
        return as(value -> {
            try {
                return E.valueOf(type, value);
            }
            catch (IllegalArgumentException e) {
                throw new ClientException("An invalid value was provided for the "+name+" parameter, accepted values are "+enumNames(type.getEnumConstants())+".", e);
            }
        }, defaultValue);
    }

    /**
     * 
     * @param mapper
     * @param <T>
     * @return
     * @throws ClientException
     */
    public <T> T as(Mapper<T> mapper) throws ClientException
    {
        return this.as(mapper, null);
    }

    /**
     * 
     * @param mapper
     * @param defaultValue
     * @param <T>
     * @return
     * @throws ClientException
     */
    public <T> T as(Mapper<T> mapper, T defaultValue) throws ClientException
    {
        return this.rawValue != null ? mapper.map(this.rawValue) : defaultValue;
    }

    public interface Mapper <T>
    {
        T map(String input) throws ClientException;
    }

    /**
     *
     * @param values
     * @param <E>
     * @return
     */
    private static <E extends Enum<E>> String enumNames(E[] values)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < values.length ; ++i)
        {
            sb.append("'").append(values[i].name()).append("'");
            if (i == (values.length - 2))
                sb.append(" or ");
            else if (i < (values.length - 2))
                sb.append(", ");
        }
        return sb.toString();
    }
}
