package com.noleme.amaebi.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.wrapper.Unwrapper;
import com.noleme.amaebi.wrapper.UpdateUnwrapper;
import com.noleme.amaebi.wrapper.json.JsonUnwrapper;
import com.noleme.amaebi.wrapper.json.JsonUpdateUnwrapper;
import com.noleme.json.Json;
import com.noleme.json.JsonException;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.PathTemplateMatch;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * TODO: This should definitely not be an abstract class, will be reworked later.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public abstract class Controller
{
    /**
     *
     * @param exchange
     * @param name
     * @return
     */
    protected static Parameter pathParam(final HttpServerExchange exchange, String name)
    {
        PathTemplateMatch pathMatch = exchange.getAttachment(PathTemplateMatch.ATTACHMENT_KEY);
        return new Parameter(name, pathMatch.getParameters().get(name));
    }

    /**
     *
     * @param exchange
     * @param name
     * @return
     */
    protected static Parameter queryParam(final HttpServerExchange exchange, String name)
    {
        Deque<String> qp = exchange.getQueryParameters().get(name);
        return new Parameter(name, qp != null ? qp.getFirst() : null);
    }

    /**
     *
     * @param exchange
     * @param name
     * @return
     */
    protected static List<Parameter> queryParams(final HttpServerExchange exchange, String name)
    {
        List<Parameter> parameters = new ArrayList<>();
        Deque<String> qp = exchange.getQueryParameters().get(name);
        if (qp != null)
        {
            for (String param : qp)
                parameters.add(new Parameter(name, param));
        }
        return parameters;
    }

    /**
     *
     * @param exchange
     * @param unwrapper
     * @param <T>
     * @return
     */
    protected static <T> T unwrapPayload(final HttpServerExchange exchange, Unwrapper<InputStream, T> unwrapper) throws ClientException
    {
        if (!exchange.isBlocking())
            throw new IllegalStateException("The payload cannot be extracted from a non-blocking context.");

        return unwrapper.unwrap(exchange.getInputStream());
    }

    /**
     *
     * @param exchange
     * @param unwrapper
     * @param target
     * @param <T>
     * @return
     */
    protected static <T> T unwrapPayload(final HttpServerExchange exchange, UpdateUnwrapper<InputStream, T> unwrapper, T target) throws ClientException
    {
        if (!exchange.isBlocking())
            throw new IllegalStateException("The payload cannot be extracted from a non-blocking context.");

        return unwrapper.unwrap(exchange.getInputStream(), target);
    }

    /**
     *
     * @param exchange
     * @param unwrapper
     * @param <T>
     * @return
     * @throws ClientException
     */
    protected static <T> T unwrapJsonPayload(final HttpServerExchange exchange, JsonUnwrapper<T> unwrapper) throws ClientException
    {
        if (!exchange.isBlocking())
            throw new IllegalStateException("The payload cannot be extracted from a non-blocking context.");

        try {
            JsonNode node = Json.parse(exchange.getInputStream());

            return unwrapper.unwrap(node);
        }
        catch (JsonException e) {
            throw new ClientException("The payload could not be interpreted as valid JSON.");
        }
    }

    /**
     *
     * @param exchange
     * @param unwrapper
     * @param target
     * @param <T>
     * @return
     * @throws ClientException
     */
    protected static <T> T unwrapJsonPayload(final HttpServerExchange exchange, JsonUpdateUnwrapper<T> unwrapper, T target) throws ClientException
    {
        if (!exchange.isBlocking())
            throw new IllegalStateException("The payload cannot be extracted from a non-blocking context.");

        try {
            JsonNode node = Json.parse(exchange.getInputStream());

            return unwrapper.unwrap(node, target);
        }
        catch (JsonException e) {
            throw new ClientException("The payload could not be interpreted as valid JSON.");
        }
    }
}
