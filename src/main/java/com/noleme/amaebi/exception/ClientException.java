package com.noleme.amaebi.exception;

import com.noleme.amaebi.http.Status;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class ClientException extends AppException
{
    public ClientException(String message)
    {
        super(message, Status.BAD_REQUEST);
    }

    public ClientException(String message, Throwable cause)
    {
        super(message, cause, Status.BAD_REQUEST);
    }

    public ClientException(String message, Status code)
    {
        super(message, code);
    }

    public ClientException(String message, Throwable cause, Status code)
    {
        super(message, cause, code);
    }
}
