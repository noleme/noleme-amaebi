package com.noleme.amaebi.exception;

import com.noleme.amaebi.http.Status;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class NotFoundException extends AppException
{
    public NotFoundException(String message)
    {
        super(message, Status.NOT_FOUND);
    }

    public NotFoundException(String message, Throwable cause)
    {
        super(message, cause, Status.NOT_FOUND);
    }
}
