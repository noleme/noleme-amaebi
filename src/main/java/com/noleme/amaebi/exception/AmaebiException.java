package com.noleme.amaebi.exception;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 16/12/2019
 */
public class AmaebiException extends Exception
{
    /**
     *
     * @param message
     */
    public AmaebiException(String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public AmaebiException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
