package com.noleme.amaebi.exception;

import com.noleme.amaebi.http.Status;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class AppException extends Exception
{
    private final Status status;

    public AppException(String message, Status status)
    {
        super(message);
        this.status = status;
    }

    public AppException(String message, Throwable cause, Status status)
    {
        super(message, cause);
        this.status = status;
    }

    public Status status()
    {
        return this.status;
    }
}
