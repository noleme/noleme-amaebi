package com.noleme.amaebi.exception;

import com.noleme.amaebi.http.Status;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class ServerException extends AppException
{
    public ServerException(String message)
    {
        super(message, Status.INTERNAL_SERVER_ERROR);
    }

    public ServerException(String message, Throwable cause)
    {
        super(message, cause, Status.INTERNAL_SERVER_ERROR);
    }

    public ServerException(String message, Status code)
    {
        super(message, code);
    }

    public ServerException(String message, Throwable cause, Status code)
    {
        super(message, cause, code);
    }
}
