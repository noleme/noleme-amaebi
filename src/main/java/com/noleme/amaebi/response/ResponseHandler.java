package com.noleme.amaebi.response;

import com.noleme.amaebi.http.Status;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface ResponseHandler <P>
{
    /**
     *
     * @param exchange
     * @param payload
     */
    void result(final HttpServerExchange exchange, P payload);

    /**
     *
     * @param exchange
     * @param payload
     * @param code
     */
    void result(final HttpServerExchange exchange, P payload, Status code);
}
