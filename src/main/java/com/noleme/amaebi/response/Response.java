package com.noleme.amaebi.response;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.http.ContentType;
import com.noleme.amaebi.http.Status;
import com.noleme.json.Json;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 08/11/2020
 */
public final class Response
{
    private Response() {}

    /**
     *
     * @param exchange
     * @param payload
     */
    public static void send(final HttpServerExchange exchange, String payload)
    {
        send(exchange, payload, Status.OK);
    }

    /**
     *
     * @param exchange
     * @param payload
     * @param code
     */
    public static void send(final HttpServerExchange exchange, String payload, Status code)
    {
        send(exchange, payload, code, ContentType.TextPlain);
    }

    /**
     *
     * @param exchange
     * @param payload
     * @param code
     * @param contentType
     */
    public static void send(final HttpServerExchange exchange, String payload, Status code, ContentType contentType)
    {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, contentType.getMimeType());
        exchange.setStatusCode(code.getCode());
        exchange.getResponseSender().send(payload);
    }

    /**
     *
     * @param exchange
     * @param payload
     */
    public static void send(final HttpServerExchange exchange, JsonNode payload)
    {
        send(exchange, payload, Status.OK);
    }

    /**
     *
     * @param exchange
     * @param payload
     * @param code
     */
    public static void send(final HttpServerExchange exchange, JsonNode payload, Status code)
    {
        send(exchange, Json.stringify(payload), code, ContentType.ApplicationJson);
    }
}
