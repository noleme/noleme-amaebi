package com.noleme.amaebi.response;

import com.noleme.amaebi.http.Status;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface ErrorHandler <E extends Throwable>
{
    /**
     *
     * @param exchange
     * @param e
     */
    void error(final HttpServerExchange exchange, E e);

    /**
     *
     * @param exchange
     * @param e
     * @param code
     */
    void error(final HttpServerExchange exchange, E e, Status code);
}
