package com.noleme.amaebi.response;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.noleme.amaebi.http.Status;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public interface JsonResponseHandler <P> extends ResponseHandler<P>
{
    /**
     *
     * @param exchange
     * @param status
     */
    void status(final HttpServerExchange exchange, ObjectNode status);

    /**
     *
     * @param exchange
     * @param status
     * @param code
     */
    void status(final HttpServerExchange exchange, ObjectNode status, Status code);

    /**
     *
     * @param exchange
     * @param payload
     * @param status
     */
    void result(final HttpServerExchange exchange, P payload, ObjectNode status);

    /**
     *
     * @param exchange
     * @param payload
     * @param status
     * @param code
     */
    void result(final HttpServerExchange exchange, P payload, ObjectNode status, Status code);
}
