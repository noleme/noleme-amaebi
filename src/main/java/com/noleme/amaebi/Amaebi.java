package com.noleme.amaebi;

import com.noleme.amaebi.configuration.AmaebiConfiguration;
import com.noleme.amaebi.exception.AmaebiException;
import com.noleme.amaebi.routing.Routing;
import io.undertow.Undertow;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 14/12/2019
 */
public class Amaebi
{
    private final AmaebiConfiguration configuration;
    private final Routing routing;
    private Undertow server;
    private ServerStatus status;

    /**
     *
     * @param configuration
     */
    public Amaebi(final AmaebiConfiguration configuration)
    {
        this.configuration = configuration;
        this.routing = new Routing();
        this.status = ServerStatus.INERT;
    }

    /**
     *
     * @return
     */
    public Routing routing()
    {
        return this.routing;
    }

    /**
     *
     * @return
     */
    synchronized public Amaebi build() throws AmaebiException
    {
        if (this.status == ServerStatus.RUNNING)
            throw new AmaebiException("The server is currently running and cannot be re-built, make sure the server is stopped before calling Amaebi.build() again.");

        this.server = this.configuration.builder()
            .setHandler(this.routing.build().handler())
            .build()
        ;
        this.status = ServerStatus.BUILT;

        return this;
    }

    /**
     *
     * @return
     */
    synchronized public Amaebi start() throws AmaebiException
    {
        if (this.status == ServerStatus.INERT)
            throw new AmaebiException("The server is currently inert and has to be built before running.");
        if (this.status == ServerStatus.RUNNING)
            throw new AmaebiException("The server is currently running and has to be stopped before calling Amaebi.start() again.");

        this.server.start();
        this.status = ServerStatus.RUNNING;

        return this;
    }

    /**
     *
     * @return
     */
    synchronized public Amaebi stop() throws AmaebiException
    {
        if (this.status == ServerStatus.INERT)
            throw new AmaebiException("The server is currently inert and has to be built before stopping.");

        this.server.stop();
        this.status = ServerStatus.BUILT;

        return this;
    }

    public enum ServerStatus
    {
        INERT,
        BUILT,
        RUNNING,
        ;
    }

    public ServerStatus status()
    {
        return this.status;
    }
}
