package response;

import com.noleme.amaebi.exception.AppException;
import com.noleme.amaebi.response.JsonResponseHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class AppExceptionHandler extends DefaultErrorHandler<AppException>
{
    /**
     * @param handler
     * @param printStackTraces
     */
    public AppExceptionHandler(JsonResponseHandler handler, boolean printStackTraces)
    {
        super(handler, printStackTraces);
    }

    @Override
    public void error(HttpServerExchange exchange, AppException e)
    {
        this.error(exchange, e, e.status());
    }
}
