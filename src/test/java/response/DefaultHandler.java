package response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.noleme.amaebi.http.Status;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.json.Json;
import handler.CounterHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class DefaultHandler implements JsonResponseHandler<JsonNode>
{
    @Override
    public void status(HttpServerExchange exchange, ObjectNode status)
    {
        this.status(exchange, status, Status.OK);
    }

    @Override
    public void status(HttpServerExchange exchange, ObjectNode status, Status code)
    {
        this.result(exchange, null, status, code);
    }

    @Override
    public void result(HttpServerExchange exchange, JsonNode payload, ObjectNode status)
    {
        this.result(exchange, payload, Status.OK);
    }

    @Override
    public void result(HttpServerExchange exchange, JsonNode payload, ObjectNode status, Status code)
    {
        ObjectNode json = Json.newObject();

        json.set("status", statusContainer(exchange, status));

        if (payload != null)
            json.set("payload", payload);

        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        exchange.setStatusCode(code.getCode());
        exchange.getResponseSender().send(json.toString());
    }

    @Override
    public void result(HttpServerExchange exchange, JsonNode payload)
    {
        this.result(exchange, payload, Status.OK);
    }

    @Override
    public void result(HttpServerExchange exchange, JsonNode payload, Status code)
    {
        this.result(exchange, payload, Json.newObject(), code);
    }

    /**
     *
     * @param status
     * @return
     */
    protected JsonNode statusContainer(HttpServerExchange exchange, ObjectNode status)
    {
        if (status == null)
            status = Json.newObject();

        if (!status.has("uri") && exchange.getRequestURI() != null)
            status.put("uri", exchange.getRequestURI());

        Integer count = exchange.getAttachment(CounterHandler.COUNTER_KEY);
        if (count != null)
            status.put("counter", count);

        return status;
    }
}
