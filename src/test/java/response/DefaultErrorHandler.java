package response;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.noleme.amaebi.http.Status;
import com.noleme.amaebi.response.ErrorHandler;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.json.Json;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public abstract class DefaultErrorHandler <E extends Throwable> implements ErrorHandler<E>
{
    private final JsonResponseHandler handler;
    private final boolean printStackTraces;

    /**
     *
     * @param handler
     * @param printStackTraces
     */
    public DefaultErrorHandler(JsonResponseHandler handler, final boolean printStackTraces)
    {
        this.handler = handler;
        this.printStackTraces = printStackTraces;
    }

    @Override
    public void error(HttpServerExchange exchange, E e, Status code)
    {
        if (this.printStackTraces)
            e.printStackTrace();

        ObjectNode status = Json.newObject()
            .put("message", "Error")
            .put("description", e.getMessage())
        ;

        this.handler.status(exchange, status, code);
    }
}
