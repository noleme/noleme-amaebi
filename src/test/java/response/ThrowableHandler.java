package response;

import com.noleme.amaebi.http.Status;
import com.noleme.amaebi.response.JsonResponseHandler;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class ThrowableHandler extends DefaultErrorHandler<Throwable>
{
    /**
     * @param handler
     * @param printStackTraces
     */
    public ThrowableHandler(JsonResponseHandler handler, boolean printStackTraces)
    {
        super(handler, printStackTraces);
    }

    @Override
    public void error(HttpServerExchange exchange, Throwable e)
    {
        this.error(exchange, e, Status.INTERNAL_SERVER_ERROR);
    }
}
