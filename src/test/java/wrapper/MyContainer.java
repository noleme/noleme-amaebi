package wrapper;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class MyContainer
{
    private final String name;
    private String argument;

    public MyContainer(String name, String argument)
    {
        this.name = name;
        this.argument = argument;
    }

    public String getName()
    {
        return this.name;
    }

    public String getArgument()
    {
        return this.argument;
    }

    public MyContainer setArgument(String argument)
    {
        this.argument = argument;
        return this;
    }
}
