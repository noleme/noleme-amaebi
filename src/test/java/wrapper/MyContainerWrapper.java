package wrapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.wrapper.json.JsonUnwrapper;
import com.noleme.amaebi.wrapper.json.JsonUpdateUnwrapper;
import com.noleme.amaebi.wrapper.json.JsonWrapper;
import com.noleme.json.Json;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class MyContainerWrapper implements JsonWrapper<MyContainer>, JsonUnwrapper<MyContainer>, JsonUpdateUnwrapper<MyContainer>
{
    @Override
    public JsonNode wrap(MyContainer input)
    {
        return Json.newObject()
            .put("name", input.getName())
            .put("argument", input.getArgument())
        ;
    }

    @Override
    public MyContainer unwrap(JsonNode input) throws ClientException
    {
        if (!input.has("name") || !input.has("argument"))
            throw new ClientException("The \"name\" and \"argument\" properties are required.");

        return new MyContainer(
            input.get("name").asText(),
            input.get("argument").asText()
        );
    }

    @Override
    public MyContainer unwrap(JsonNode input, MyContainer preexisting) throws ClientException
    {
        if (input.has("argument"))
            preexisting.setArgument(input.get("argument").asText());

        return preexisting;
    }
}
