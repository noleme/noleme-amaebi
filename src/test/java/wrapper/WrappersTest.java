package wrapper;

import com.noleme.amaebi.wrapper.Wrappers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 11/06/2020
 */
public class WrappersTest
{
    @Test
    void wrapInstantTest()
    {
        Instant a = Instant.parse("2013-02-03T12:45:19.00Z");
        Instant b = Instant.parse("2020-12-03T12:45:19.00Z");

        Assertions.assertEquals("2013-02-03T12:45:19Z", Wrappers.wrap(a));
        Assertions.assertEquals("2020-12-03T12:45:19Z", Wrappers.wrap(b));
    }
}
