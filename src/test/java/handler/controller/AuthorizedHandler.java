package handler.controller;

import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.handler.controller.AnnotationHandler;
import com.noleme.amaebi.http.Status;
import controller.annotation.Authorized;
import io.undertow.server.HttpServerExchange;

import java.lang.reflect.Method;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class AuthorizedHandler extends AnnotationHandler<Authorized>
{
    /**
     *
     */
    public AuthorizedHandler()
    {
        super(Authorized.class);
    }

    @Override
    public void handleAnnotation(HttpServerExchange exchange, Method method, Authorized annotation) throws ClientException
    {
        System.out.println("This method requires role: "+annotation.role());
        if (annotation.role() == Authorized.Role.ADMIN)
            throw new ClientException("Unauthorized access.", Status.UNAUTHORIZED);
    }
}
