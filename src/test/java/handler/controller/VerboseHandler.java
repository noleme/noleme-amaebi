package handler.controller;

import com.noleme.amaebi.handler.controller.AnnotationHandler;
import controller.annotation.Verbose;
import io.undertow.server.HttpServerExchange;

import java.lang.reflect.Method;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class VerboseHandler extends AnnotationHandler<Verbose>
{
    /**
     *
     */
    public VerboseHandler()
    {
        super(Verbose.class);
    }

    @Override
    public void handleAnnotation(HttpServerExchange exchange, Method method, Verbose annotation) throws Exception
    {
        switch (annotation.level())
        {
            case SILENT: System.out.println("Shhhh."); break;
            case MIDDLING: System.out.println("We are accessing: "+method.getName()); break;
            case LOUD: System.out.println("WE ARE ACCESSING: "+method.getName().toUpperCase()); break;
            default: break;
        }
    }
}
