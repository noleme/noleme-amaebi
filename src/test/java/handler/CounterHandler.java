package handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.AttachmentKey;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
public class CounterHandler implements HttpHandler
{
    public static final AttachmentKey<Integer> COUNTER_KEY = AttachmentKey.create(Integer.class);

    @Override
    public void handleRequest(HttpServerExchange exchange)
    {
        Integer count = exchange.getAttachment(COUNTER_KEY);
        if (count == null)
            count = 0;
        exchange.putAttachment(COUNTER_KEY, count + 1);
    }
}
