package controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.controller.Controller;
import com.noleme.amaebi.controller.annotation.Blocking;
import com.noleme.amaebi.controller.annotation.Route;
import com.noleme.amaebi.controller.annotation.RoutePrefix;
import com.noleme.amaebi.exception.ClientException;
import com.noleme.amaebi.exception.NotFoundException;
import com.noleme.amaebi.exception.ServerException;
import com.noleme.amaebi.http.Method;
import com.noleme.amaebi.http.Status;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.commons.container.Maps;
import com.noleme.json.Json;
import controller.annotation.Verbose;
import io.undertow.server.HttpServerExchange;
import wrapper.MyContainer;
import wrapper.MyContainerWrapper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
@RoutePrefix(prefix = "/my-container")
public class MyContainerController extends Controller
{
    private final MyContainerWrapper wrapper = new MyContainerWrapper();
    private final Map<String, MyContainer> store = new ConcurrentHashMap<>();
    private final JsonResponseHandler<JsonNode> response;

    /**
     *
     * @param responseHandler
     */
    public MyContainerController(JsonResponseHandler<JsonNode> responseHandler)
    {
        this.response = responseHandler;
        this.store.putAll(Maps.of(
            "meuh", new MyContainer("meuh", "some-value"),
            "moo", new MyContainer("moo", "une-valeur")
        ));
    }

    @Route(method = Method.GET, path = "/anything-goes")
    @Route(method = Method.GET, path = "/anything-goes-bis")
    public void anythingGoes(HttpServerExchange exchange)
    {
        System.out.println("blocking: "+exchange.isBlocking());
        this.response.status(exchange, Json.newObject().put("anything_goes", true));
    }

    @Route(method = Method.GET, path = "/anything-goes-ter")
    @Blocking
    public void anythingGoesTer(HttpServerExchange exchange)
    {
        System.out.println("blocking: "+exchange.isBlocking());
        this.response.status(exchange, Json.newObject().put("anything_goes", true));
    }

    @Verbose(level = Verbose.Level.SILENT)
    @Route(method = Method.GET, path = "/{name}")
    @Blocking
    public void get(HttpServerExchange exchange) throws NotFoundException
    {
        String name = queryParam(exchange, "name").asString();

        if (!this.store.containsKey(name))
            throw new NotFoundException("Container with name \""+name+"\" could not be found.");

        this.response.result(exchange, this.wrapper.wrap(this.store.get(name)));
    }

    @Verbose(level = Verbose.Level.MIDDLING)
    @Route(method = Method.GET)
    @Blocking
    public void list(HttpServerExchange exchange) throws ServerException
    {
        this.response.result(exchange, this.wrapper.wrapJson(this.store.values()));
    }

    @Verbose(level = Verbose.Level.LOUD)
    @Route(method = Method.POST)
    @Blocking
    public void create(HttpServerExchange exchange) throws ClientException
    {
        MyContainer container = unwrapJsonPayload(exchange, wrapper);

        this.store.put(container.getName(), container);

        this.response.result(exchange, this.wrapper.wrap(container), Status.CREATED);
    }

    @Verbose(level = Verbose.Level.LOUD)
    @Route(method = Method.PATCH, path = "/{name}")
    @Blocking
    public void update(HttpServerExchange exchange) throws NotFoundException, ClientException
    {
        String name = queryParam(exchange, "name").asString();

        if (!store.containsKey(name))
            throw new NotFoundException("Container with name \""+name+"\" could not be found.");

        MyContainer container = unwrapJsonPayload(exchange, this.wrapper, this.store.get(name));

        this.response.result(exchange, this.wrapper.wrap(container));
    }

    @Verbose(level = Verbose.Level.LOUD)
    @Route(method = Method.DELETE, path = "/{name}")
    @Blocking
    public void delete(HttpServerExchange exchange) throws NotFoundException
    {
        String name = queryParam(exchange, "name").asString();

        if (!store.containsKey(name))
            throw new NotFoundException("Container with name \""+name+"\" could not be found.");

        this.store.remove(name);

        this.response.status(exchange, Json.newObject());
    }
}
