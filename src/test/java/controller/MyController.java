package controller;

import com.noleme.amaebi.controller.annotation.Route;
import com.noleme.amaebi.controller.annotation.RoutePrefix;
import com.noleme.amaebi.http.Method;
import io.undertow.server.HttpServerExchange;

import static com.noleme.amaebi.response.Response.send;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 31/03/2021
 */
@RoutePrefix(prefix = "/world")
public class MyController
{
    @Route(method = Method.GET, path = "/hello")
    public void helloWorld(HttpServerExchange ex)
    {
        send(ex, "Hello world!");
    }

    @Route(method = Method.POST, path = "/goodbye")
    public void goodByeWorld(HttpServerExchange ex)
    {
        send(ex, "Goodbye world!");
    }
}
