package controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.json.Json;
import io.undertow.server.HttpHandler;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 12/01/2020
 */
public class MyOldController
{
    private final JsonResponseHandler<JsonNode> response;

    /**
     *
     */
    public MyOldController(JsonResponseHandler<JsonNode> responseHandler)
    {
        this.response = responseHandler;
    }

    /**
     *
     * @return
     */
    public static HttpHandler staticEquivalent(JsonResponseHandler<JsonNode> response)
    {
        return exchange -> {
            System.out.println("blocking: "+exchange.isBlocking());
            response.status(exchange, Json.newObject().put("old", true));
        };
    }

    /**
     *
     * @return
     */
    public HttpHandler instanceEquivalent()
    {
        return exchange -> {
            System.out.println("blocking: "+exchange.isBlocking());
            this.response.status(exchange, Json.newObject().put("old", true));
        };
    }
}
