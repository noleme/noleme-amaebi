package controller.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Verbose
{
    Verbose.Level level();

    enum Level
    {
        SILENT,
        MIDDLING,
        LOUD;
    }
}
