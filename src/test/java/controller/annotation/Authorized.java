package controller.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 15/12/2019
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorized
{
    Role role();

    enum Role
    {
        ANONYMOUS("ROLE_ANONYMOUS"),
        USER("ROLE_USER"),
        MANAGER("ROLE_MANAGER"),
        ADMIN("ROLE_ADMIN"),
        SUPERADMIN("ROLE_SUPERADMIN"),
        SEARCH_USER("ROLE_SEARCH_USER"),
        NEWS_USER("ROLE_NEWS_USER");

        private String role;

        Role(String role)
        {
            this.role = role;
        }

        public String getRole()
        {
            return this.role;
        }
    }
}
