package controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.controller.Controller;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.json.Json;
import io.undertow.server.HttpServerExchange;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 12/01/2020
 */
public class MyConsumerController extends Controller
{
    private final JsonResponseHandler<JsonNode> response;

    /**
     *
     */
    public MyConsumerController(JsonResponseHandler<JsonNode> responseHandler)
    {
        this.response = responseHandler;
    }

    /**
     *
     * @param exchange
     */
    public void consumerTest(HttpServerExchange exchange)
    {
        System.out.println("blocking: "+exchange.isBlocking());
        this.response.status(exchange, Json.newObject().put("consumer", true));
    }
}
