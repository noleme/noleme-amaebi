import com.fasterxml.jackson.databind.JsonNode;
import com.noleme.amaebi.Amaebi;
import com.noleme.amaebi.configuration.AmaebiLoader;
import com.noleme.amaebi.exception.AmaebiException;
import com.noleme.amaebi.exception.AppException;
import com.noleme.amaebi.handler.ExceptionHandler;
import com.noleme.amaebi.handler.NotFoundHandler;
import com.noleme.amaebi.http.Method;
import com.noleme.amaebi.http.Status;
import com.noleme.amaebi.response.ErrorHandler;
import com.noleme.amaebi.response.JsonResponseHandler;
import com.noleme.json.Json;
import controller.MyConsumerController;
import controller.MyContainerController;
import controller.MyController;
import controller.MyOldController;
import handler.CounterHandler;
import handler.controller.AuthorizedHandler;
import handler.controller.VerboseHandler;
import response.AppExceptionHandler;
import response.DefaultHandler;
import response.ThrowableHandler;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/12/2019
 */
public class ServerTest
{
    public static void main(String[] args) throws AmaebiException, InterruptedException
    {
        Amaebi server = new Amaebi(AmaebiLoader.load("amaebi.conf"));

        JsonResponseHandler<JsonNode> defaultHandler = new DefaultHandler();
        ErrorHandler<AppException> appExceptionErrorHandler = new AppExceptionHandler(defaultHandler, false);
        ErrorHandler<Throwable> throwableErrorHandler = new ThrowableHandler(defaultHandler, true);

        MyConsumerController consumerController = new MyConsumerController(defaultHandler);
        MyOldController oldController = new MyOldController(defaultHandler);

        server.routing()
            .register(new MyController())
            .register(new MyContainerController(defaultHandler), "/api")
            .register(Method.GET, "/api/consumer/reference", consumerController::consumerTest)
            .register(Method.GET, "/api/test.json", exchange -> {
                defaultHandler.status(exchange, Json.newObject().put("test", true), Status.ACCEPTED);
            })
            .register(Method.GET, "/api/old/instance", oldController.instanceEquivalent())
            .register(Method.GET, "/api/old/static", MyOldController.staticEquivalent(defaultHandler))
            .setNotFoundHandler(new NotFoundHandler())
            .addErrorHandler(AppException.class, new ExceptionHandler<>(appExceptionErrorHandler))
            .addErrorHandler(Throwable.class, new ExceptionHandler<>(throwableErrorHandler))
            .addRequestHandler(new CounterHandler())
            /*.addRequestHandler(exchange -> {
                throw new ClientException("Bad thing happened");
            })*/
            .addRequestHandler(new CounterHandler())
            .addRequestHandler(new CounterHandler())
            .addControllerHandler(new AuthorizedHandler())
            .addControllerHandler(new VerboseHandler())
            /*.addRequestHandler(exchange -> {
                throw new ServerException("Bad thing happened");
            })*/
        ;

        server.build().start();
    }
}
